#!/usr/bin/env bash
# Installing the AWS CLI and Python SDK
# These commands are intended to be run on on MacOS

export WHOAMI=$(whoami);
export cmd=$1

uninstall() {
    echo ">>>>> Removing old AWS (1 of 2)>>>>>"
    sudo rm -rf /usr/local/aws
    echo ">>>>> Removing old AWS (2 of 2)>>>>>"
    sudo rm /usr/local/bin/aws
}

#TODO: Fix
#Chad: I noticed on my fresh install of the awscli, it wants me to add them all to git
#i didn't, but just an fyi about putting it in that directory that it'll need to be handled
#https://lh3.googleusercontent.com/-2o6wheaPtCo/W6UORvrZU6I/AAAAAAABvNg/1yDkci3Z_eQHVy-WMBTh9Lk3QV-iHJvQgCL0BGAYYCw/h199/2018-09-21.png

install_aws() {
    #install_prerequisites
    echo ">>>>>Checking/installing Python >>>>>>"
    hash python 2>/dev/null || { brew install -y python; }
    echo ">>>>>Checking/installing Curl >>>>>>"
    hash curl 2>/dev/null || { brew install -y curl; }

    download_aws
    unzip_aws
    install_aws_local
        #Ref: https://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html
}

download_aws() {
    hash aws 2>/dev/null || {
            echo ">>>>>Downloading AWS CLI >>>>>>>"
            curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
        }
}

unzip_aws() {
    hash aws 2>/dev/null || {
            echo ">>>>>Unzipping AWS CLI >>>>>>>"
            unzip awscli-bundle.zip
        }
}

install_aws_local() {
hash aws 2>/dev/null || {
            echo ">>>>>Installing AWS CLI >>>>>>>"
            sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
            aws --version
        }

}

configure_aws() {
    echo "*****Get your Access key ID & Secret Access key ID. Instructions:"
            echo " https://docs.google.com/document/d/1Y-urGH6KT63X5VpSbjlb9W_hfuGhU9DaIQLAn_JvcGg/edit?usp=sharing"
            echo "           then use: "
            echo "                region=us-east-1"
            echo "                output=json"
            aws configure
}

install_docker() {
    hash docker 2>/dev/null || {
                echo ">>>>>>>Installing Docker for Mac>>>>>>>"
                curl -o ~/Downloads/Docker.dmg https://download.docker.com/mac/stable/Docker.dmg;
                open ~/Downloads/Docker.dmg;
                docker_instruction="${WHOAMI}, Complete Docker for Mac installation on your screen. After installation, Docker preferences for large enterprise apps \
                may require 5 gigs of memory and 1.5  gigs of swap. Run your enterprise app natively and analyse host's resource consumption for exact numbers."
                say ${docker_instruction};
                echo ${docker_instruction};
            }

}

install_cfn_lint(){
#    https://github.com/aws-cloudformation/cfn-python-lint

    hash cfn-lint 2>/dev/null || {
                echo ">>>>>>>Installing cfn-lint>>>>>>>"
                pip install cfn-lint --user
                cfn_message="cfn-lint install complete"
                say ${cfn_message};
                echo ${cfn_message} && echo "Documentation:" && echo "https://github.com/aws-cloudformation/cfn-python-lint";
            }
}

install_aws_tools() {
    install_docker
    install_cfn_lint

    hash ecs-cli 2>/dev/null || {
    #TODO create method install_ecs()
        echo ">>>>>>>Installing AWS ecs-cli>>>>>>>"
        sudo curl -o /usr/local/bin/ecs-cli https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-darwin-amd64-latest
        echo "Verify server MD5 matches, local MD5"
        curl -s https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-darwin-amd64-latest.md5 && md5 -q /usr/local/bin/ecs-cli
        echo "Applying execute permission to binary"
        sudo chmod +x /usr/local/bin/ecs-cli
        echo "ECS CLI installed/upgrade successfully to: "
        ecs-cli --version
    }

    #Required to build Cassandra image
    hash gpg 2>/dev/null || {
        brew update
        brew install -v gpg
    }

    whatToPrompt="${WHOAMI}, your local automated serverless setup is complete or upgraded. Verify no terminal errors. Do what's listed below manually."

    say ${whatToPrompt}
    echo ${whatToPrompt}

}

install_oracle_client() {
    echo "MANUAL SETUP TOOLS"
    echo "1) Install SQL*Plus"
    echo " # download basic & sqlplus zip files into ~/Library/Caches/Homebrew & install"
    echo "      1.1) cd ~/Library/Caches/Homebrew"
    echo "      1.2) Download http://download.oracle.com/otn/mac/instantclient/122010/instantclient-basic-macos.x64-12.2.0.1.0-2.zip"
    echo "      1.3) Download http://download.oracle.com/otn/mac/instantclient/122010/instantclient-sqlplus-macos.x64-12.2.0.1.0-2.zip"
    echo "      1.4) Run: "
    echo "              brew tap InstantClientTap/instantclient"
    echo "              brew install instantclient-basic"
    echo "              brew install instantclient-sqlplus"
    echo "              sqlplus -version "
    echo "              export ORACLE_HOME=/u01/app/oracle/product/12.2.0/EE"
    echo "                  export ORACLE_SID=EE"
    echo "                  export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/u01/app/oracle/product/12.2.0/EE/bin"
    echo "2) Optional, install a more readable shell, eg., http://ohmyz.sh/ "

}

#Do not run this unless you work with these tools.
install_optional_tools() {
    check_install_jq
    echo ">>>>>If not installed, installing nose >>>>>>"
    hash nose 2>/dev/null || { sudo easy_install nose; }
    echo ">>>>>If not installed, installing tornado >>>>>>"
    hash tornado 2>/dev/null || { sudo easy_install tornado; }
    # once python & curl are installed, install PIP
    echo ">>>>>If not installed, installing pip >>>>>>"
    hash pip 2>/dev/null || { sudo easy_install pip; }
    # install Boto3, the AWS Python SDK
    echo ">>>>>>>Installing boto3, the AWS Python SDK.>>>>>>>"
    pip install boto3
    echo ">>>>>> Installing pyboto3 for IDE auto completion"
    pip install pyboto3 --user

    install_oracle_client
}

install_prerequisites() {
    echo ">>>>>If not installed, installing brew >>>>>>"
    hash brew 2>/dev/null || { /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"; }
}

check_install_jq() {
    echo ">>>>>Checking installing jq>>>>>>"
    hash jq 2>/dev/null || { brew install -y jq; } # jq is a JSON parse and query tool, very helpful for manipulating JSON returned from AWS CLI
}


if [ "$cmd" = 'uninstall' ]; then #Run if install_aws has errors
    uninstall
elif [ "$cmd" = 'unzip_aws' ]; then
    unzip_aws
elif [ "$cmd" = 'install_aws' ]; then # Required #1
    install_aws
elif [ "$cmd" = 'configure_aws' ]; then # Required #2
    configure_aws
elif [ "$cmd" = 'install_aws_tools' ]; then # Required #3
    install_aws_tools
elif [ "$cmd" = 'install_optional_tools' ]; then
    install_optional_tools
elif [ "$cmd" = 'install_docker' ]; then
    install_docker
else
        echo ""
        echo "$1 :INVALID COMMAND."
        exit 1
fi

# Checking if ecs-cli --version matches latest git repo
# TODO. Check if ecs-cli --version != https://github.com/aws/amazon-ecs-cli/blob/master/VERSION then re-run installation
# install_ecs(). kinda sucks there's no pip install --upgrade ecs-cli


# TODO Check if aws cli update. Check local version matches git version, if not then run
#pip install awscli --upgrade --user

#In certain version of MacOS, errors can be resolved with:

#chmod +x /Library/Python/
#sudo pip install --upgrade pip
#hash python3 2>/dev/null || { brew install python3; }
#pip3 install --upgrade virtualenv
#
#hash freetype 2>/dev/null || { brew install freetype; }
#hash pkg-config 2>/dev/null || { brew install pkg-config; }

#########
#sudo pip install --user s4cmd --ignore-installed six
#I get the following warning :
#The directory '/Users/cmathis/Library/Caches/pip/http' or its parent directory is not owned by the current user and the cache has been disabled. Please check the permissions and owner of that directory. If executing pip with sudo, you may want sudo's -H flag.
#The directory '/Users/cmathis/Library/Caches/pip' or its parent directory is not owned by the current user and caching wheels has been disabled. check the permissions and owner of that directory. If executing pip with sudo, you may want sudo's -H flag.
#but it installs ok
###########


#chmod +x /usr/local/bin/s4cmd
